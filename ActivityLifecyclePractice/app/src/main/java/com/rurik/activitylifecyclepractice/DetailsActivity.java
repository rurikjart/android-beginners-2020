package com.rurik.activitylifecyclepractice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    private static final String LOG_TAG = "dc.DetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Log.d(LOG_TAG, "onCreate()");

        Intent intent = getIntent();
        String login = intent.getStringExtra(MainActivity.EXTRA_LOGIN);
        String password = intent.getStringExtra(MainActivity.EXTRA_PASSWORD);

        TextView loginTextView = findViewById(R.id.login_text_view);
        loginTextView.setText(login);
        TextView passTextView = findViewById(R.id.password_text_view);
        passTextView.setText(password);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, "onRestart()");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy()");
    }

}
