package com.rurik.weatherloaderhttp.network;


import android.net.Uri;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClient {

    private static final String WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/weather";
    private static final String QUERY_PARAM      = "q";
    private static final String APP_ID_PARAM     = "appid";
    private static final String APP_ID           = "a7315ad7851af7676a75902ed0cebf73";

    public String getWeather(String city) throws IOException {


        String requestUrl = Uri.parse(WEATHER_BASE_URL)
                          // Создаём Builder для Uri
                                    .buildUpon()
                          // Добавляем параметр и значение
                                    .appendQueryParameter(QUERY_PARAM, city)
                                    .appendQueryParameter(APP_ID_PARAM, APP_ID)
                                    .build()
                                    .toString();






        // подключение к серверу
        URL url = new URL(requestUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        // Метод GET используется по умолчанию, поэтому можем не указывать
        // connection.setRequestMethod("GET");

        try {
            connection.connect();

            InputStream in;
            int status = connection.getResponseCode();

            if (status == HttpURLConnection.HTTP_OK) {
                in = connection.getInputStream();
            } else {
                in = connection.getErrorStream();
            }

            String responce = convertStreamToString(in);

            return  responce;

        } finally {
            connection.disconnect();
        }

    }

    private String convertStreamToString(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        stream.close();

        return sb.toString();
    }


}
