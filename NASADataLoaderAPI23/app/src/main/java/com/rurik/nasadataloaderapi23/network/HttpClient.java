package com.rurik.nasadataloaderapi23.network;

import android.net.Uri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpClient {

    private static final String PLANETARY_URL = "https://api.nasa.gov/planetary/apod";
    private static final String API_KEY_PARAM = "api_key";
    private static final String APP_ID        = "DEMO_KEY";


    public String getDayPicture() throws IOException {
        String requestUrl = Uri.parse(PLANETARY_URL)
                .buildUpon()
                .appendQueryParameter(API_KEY_PARAM, APP_ID)
                .build()
                .toString();


        // подключаемся к серверу
        return getResponse(requestUrl);

    }

    private String getResponse(String requestUrl) throws IOException {
        // подключаемся к серверу
        URL url = new URL(requestUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {
            connection.connect();

            InputStream in;
            int status = connection.getResponseCode();
            if (status == HttpURLConnection.HTTP_OK) {
                in = connection.getInputStream();
            }
            else {
                in = connection.getErrorStream();
            }

            String response = convertStreamToString(in);

            return response;

        } finally {
            connection.disconnect();
        }
    }


    private static final String NEO_URL = "https://api.nasa.gov/neo/rest/v1/neo/browse";

    public String getAsteroids() throws IOException {
        String requestUrl = Uri.parse(NEO_URL)
                .buildUpon()
                .appendQueryParameter(API_KEY_PARAM, APP_ID)
                .build()
                .toString();

        // подключаемся к серверу
        return getResponse(requestUrl);
    }





    private String convertStreamToString(InputStream stream) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        stream.close();

        return sb.toString();
    }




}
