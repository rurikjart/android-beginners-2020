package com.rurik.nasadataloaderapi23;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;

import com.rurik.nasadataloaderapi23.network.HttpClient;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private final String LOG_TAG_PH = "dc.MainActivityPhoto";
    private final String LOG_TAG_AS = "dc.MainActivityAsteroids";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final HttpClient httpClient = new HttpClient();

        new Thread(new Runnable() {

            @SuppressLint("LongLogTag")
            @Override
            public void run() {
                try {

                    String weather = httpClient.getDayPicture();
                    Log.d(LOG_TAG_PH, weather);

                    String asteroids = httpClient.getAsteroids();
                    Log.d(LOG_TAG_AS, asteroids);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
}
