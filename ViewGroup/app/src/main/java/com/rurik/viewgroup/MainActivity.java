package com.rurik.viewgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }


    public void openOrientActivity(View view){
        Intent intent = new Intent(this, OrientActivity.class);
        startActivity(intent);
    }

    public void openOrientVActivity(View view){
        Intent intent = new Intent(this, OrientVActivity.class);
        startActivity(intent);
    }

    public void openWeightActivity(View view){
        Intent intent = new Intent(this, WeightActivity.class);
        startActivity(intent);
    }

    public void openCoActivity(View view){
        Intent intent = new Intent(this, CoActivity.class);
        startActivity(intent);
    }

    public void openFrameLayoutActivity(View view){
        Intent intent = new Intent(this, FrameLayoutActivity.class);
        startActivity(intent);
    }

    public void openTask1Activity(View view){
        Intent intent = new Intent(this, Task1Activity.class);
        startActivity(intent);
    }

    public void openTask2Activity(View view){
        Intent intent = new Intent(this, Task2Activity.class);
        startActivity(intent);
    }

    public void openTask3Activity(View view){
        Intent intent = new Intent(this, Task3Activity.class);
        startActivity(intent);
    }

}
