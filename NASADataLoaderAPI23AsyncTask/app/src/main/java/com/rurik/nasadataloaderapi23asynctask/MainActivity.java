package com.rurik.nasadataloaderapi23asynctask;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.rurik.nasadataloaderapi23asynctask.network.HttpClient;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private final String LOG_TAG_PH = "dc.MainActivityPhoto";
    private final String LOG_TAG_AS = "dc.MainActivityAsteroids";

    private HttpClient httpClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        httpClient = new HttpClient();

        new GetAsteroidsAsyncTask().execute();

    }


    class GetAsteroidsAsyncTask extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... voids) {

            // выполняется в фоновом ( Background ) потоке. В нём необходимо выполнять тяжёлые операции:
            // http запросы, запросы к базе данных, работу с файлами


            try {
                String asteroids = httpClient.getAsteroids();
                return asteroids;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String resultAsteroids) {
            if (resultAsteroids != null) {
                //успешный ответ
                Toast.makeText(MainActivity.this, resultAsteroids, Toast.LENGTH_SHORT).show();
            } else {
                // Ошибка
                Toast.makeText(MainActivity.this, R.string.error_msg, Toast.LENGTH_LONG).show();
            }
        }
    }


}
