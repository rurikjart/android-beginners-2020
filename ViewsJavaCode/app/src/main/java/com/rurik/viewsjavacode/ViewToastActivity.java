package com.rurik.viewsjavacode;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ViewToastActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_toast);

        // находим кнопку по id
        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // показываем сообщение, что кнопка нажата
                Log.d("ViewToastActivity", "Кнопка нажата");
                Toast.makeText(ViewToastActivity.this, R.string.hello_world, Toast.LENGTH_SHORT).show();
            }
        });


        Toast.makeText(this, "Привет мир!", Toast.LENGTH_SHORT).show();


    }

  //  public  void  openViewToastResurceActivity(View view){
  //      Toast.makeText(this, R.string.hello_world, Toast.LENGTH_SHORT).show();
  //  }




}

