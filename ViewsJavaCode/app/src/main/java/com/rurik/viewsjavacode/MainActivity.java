package com.rurik.viewsjavacode;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public  void  openViewToastActivity(View view){
        Intent intent = new Intent(this, ViewToastActivity.class);
        startActivity(intent);
    }

    public  void  openEditTextActivity(View view){
        Intent intent = new Intent(this, EditTextActivity.class);
        startActivity(intent);
    }

    public  void  openEditInfoActivity(View view){
        Intent intent = new Intent(this, EditInfoActivity.class);
        startActivity(intent);
    }

    public  void  openChisloActivity(View view){
        Intent intent = new Intent(this, ChisloActivity.class);
        startActivity(intent);
    }

    public  void  openProgressBarActivity(View view){
        Intent intent = new Intent(this, ProgressBarActivity.class);
        startActivity(intent);
    }







}
