package com.rurik.viewsjavacode;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

public class ProgressBarActivity extends AppCompatActivity {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);

        progressBar = findViewById(R.id.progress_bar);
    }

    public void setProgressBarVisible(View v) {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void setProgressBarInvisible(View v) {
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void setProgressBarGone(View v) {
        progressBar.setVisibility(View.GONE);
    }
}
