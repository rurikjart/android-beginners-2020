package com.rurik.viewsjavacode;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChisloActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chislo);

        final EditText counterEditText = findViewById(R.id.counter_edit_text);

        Button incrementButton = findViewById(R.id.increment_button);

        incrementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // берём текст, который пользователь ввёл в EditText
                String text = counterEditText.getText().toString();
                // преобразование текста в число
                int number = Integer.valueOf(text);
                // увеличиваем число на 1
                int incrementedNumber = ++number;
                // преоброзовываем число к строке, иначе произойдёт исключение
                String numberText = String.valueOf(incrementedNumber);
                counterEditText.setText(numberText);
                Toast.makeText(ChisloActivity.this, "Вы ввели число " + text, Toast.LENGTH_SHORT).show();

            }
        });

    }
}
