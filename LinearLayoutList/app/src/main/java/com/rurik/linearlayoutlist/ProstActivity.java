package com.rurik.linearlayoutlist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prost);


        LinearLayout linearLayout = findViewById(R.id.linear_layout);

        // засекаем текущее время перед началом цикла
        long startTime = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++){

            // для примера создаём только TextView сейчас, используя конструктор
            TextView textView = new TextView(this);

            // элементы можно добавить в LinearLayout с помощью метода addView
            linearLayout.addView(textView);
            String text =  "Элемент " + i;

            // устанавливаем текст в TextView
            textView.setText(text);

        }

        // отнимаем от текущего времени время начала
        long time = System.currentTimeMillis() - startTime;
        Log.d("ProstActivity", "элементы добавились, время = " + time);

    }

}
