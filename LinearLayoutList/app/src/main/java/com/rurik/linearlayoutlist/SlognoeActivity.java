package com.rurik.linearlayoutlist;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SlognoeActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slognoe);

        LinearLayout linearLayout = findViewById(R.id.linear_layout);

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {

            // преобразуем layout в View объект
            View itemView = LayoutInflater.from(this).inflate(R.layout.item_view, linearLayout, false);

            // добавляем view в LinearLayout
            linearLayout.addView(itemView);

            // находим первый TextView, устанавливаем в него текст
            TextView firstNameTextView = itemView.findViewById(R.id.first_name_text_view);
            String firstNameText = "Имя: " + i;
            firstNameTextView.setText(firstNameText);

            // находим второй TextView, устанавливаем в него текст
            TextView lastNameTextView = itemView.findViewById(R.id.last_name_text_view);
            String lastNameText = "Фамилия: " + i;
            lastNameTextView.setText(lastNameText);
        }
        long time = System.currentTimeMillis() - startTime;
        Log.d("MainActivity", "элементы добавились, время = " + time);
    }

}