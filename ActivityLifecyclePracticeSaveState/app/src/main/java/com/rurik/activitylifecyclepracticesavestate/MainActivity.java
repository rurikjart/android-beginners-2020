package com.rurik.activitylifecyclepracticesavestate;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private  static  final String IS_TEXT_VISIBLE = "isTextVisible";

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.text_view);
        Button hideShowButton = findViewById(R.id.hide_show_button);
        hideShowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isTextVisible = textView.getVisibility() == View.VISIBLE;
                textView.setVisibility(isTextVisible ? View.GONE : View.VISIBLE);
            }
        });

        if(savedInstanceState != null) {
            boolean isTextVisible = savedInstanceState.getBoolean(IS_TEXT_VISIBLE);
            textView.setVisibility(isTextVisible ? View.VISIBLE : View.GONE);
        }


    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        boolean isTextVisible = textView.getVisibility() == View.VISIBLE;
        outState.putBoolean(IS_TEXT_VISIBLE , isTextVisible);
    }
}
