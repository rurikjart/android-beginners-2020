package com.rurik.recyclerview;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder> {

    private static final String LOG_TAG = "dc.UsersAdapter";

    private List<User> users;
    private OnUserClickListener onUserClickListener;


    public UsersAdapter(List<User> users, OnUserClickListener onUserClickListener){

        this.users = users;
        this.onUserClickListener = onUserClickListener;
    }


    public interface OnUserClickListener {

        void  onUserClick(User user);

    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // Достаём необходимые компоненты для создания View объект
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        //Создаём View объект из XML макета
        View userItemView = inflater.inflate(R.layout.user_item_view, parent, false);

        // Создаём объект UserViewHolder c Элементом userItemView
        UserViewHolder viewHolder = new UserViewHolder(userItemView);

        // логируем создание объекта контроля хеш суммы
        Log.d(LOG_TAG, "onCreateViewHolder #" + viewHolder.hashCode());


        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder userViewHolder, int position) {

        // Достаём объект User для текущей позиции
        User user = users.get(position);

        // Указываем объекту userViewHolder, чтобы он отобразил объект с данными Java объект в XML макете
        userViewHolder.bind(user);

        // логируем хеш сумму аобъекта для контроля переиспользования элементов
        Log.d(LOG_TAG, "onBindViewHolder #" + userViewHolder.hashCode());

    }

    @Override
    public int getItemCount() {
        // возвращаем количество элементов, чтобы Adapter понимал
        // сколько элементов надо создать
        return users.size();
    }


    public void addItems(List<User> newUsers) {
        // размер до добавления новых элементов, пусть будет = 5
        int sizeBeforeAdding = users.size();
        // количество элементов, которые мы добавили, пусть будет 10
        int itemCount = newUsers.size();
        users.addAll(newUsers);
        // указываем адаптеру, что вставили 10 элементов, начиная с 5 позиции
        notifyItemRangeInserted(sizeBeforeAdding, itemCount);
    }


    public class  UserViewHolder extends RecyclerView.ViewHolder{
            private TextView firstNameTextView;
            private TextView lastNameTextView;

            public UserViewHolder(@NonNull View itemView){
                super(itemView);

                firstNameTextView = itemView.findViewById(R.id.first_name_text_view);
                lastNameTextView = itemView.findViewById(R.id.last_name_text_view);

                // обработка клика по элементу списка

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int clickedPosition = getLayoutPosition();
                        User user = users.get(clickedPosition);

                        // Осталось как-то передать эту информацию в Activity
                        onUserClickListener.onUserClick(user);
                    }
                });

            }

        // Метод для отображения полей пользователя
        public void bind(User user) {
            firstNameTextView.setText(user.getFirstName());
            lastNameTextView.setText(user.getLastName());
        }
    }




}
