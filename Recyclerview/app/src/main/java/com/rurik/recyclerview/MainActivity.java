package com.rurik.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView usersRecyclerView = findViewById(R.id.users_recycler_view);
        // установка объекта LayoutManager - отвечает за форму отображения элементов
        usersRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //создаем список пользователей
       List<User>  users = createUserList();

       //на стороне активити создаем слушатель для определения переданных данных из адаптера
        UsersAdapter.OnUserClickListener listener = new UsersAdapter.OnUserClickListener() {
            @Override
            public void onUserClick(User user) {
                String fullName = user.getFirstName() + " " + user.getLastName();
                Toast.makeText(MainActivity.this, fullName, Toast.LENGTH_SHORT).show();

            }
        };


        // создаём Adapter, передавая список пользователей
        final UsersAdapter adapter = new UsersAdapter(users, listener);

        // соединяем RecyclerView и Adapter
        usersRecyclerView.setAdapter(adapter);

            // Визуальное размещение элементов списка
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        usersRecyclerView.setLayoutManager(layoutManager);

        // декоративное разделение элементов списка
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        usersRecyclerView.addItemDecoration(itemDecoration);

        // обработака по нажатию кнопки, для добавления нового элемента списка и отрисовки элемента

        Button addItemsButton = findViewById(R.id.add_items_button);
        addItemsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<User> newUsers = createUserList();
                adapter.addItems(newUsers);
            }
        });


    }

    // Метод для создания списка пользователей.
    // В реальном приложении был бы запрос к серверу или базе данных
    private List<User> createUserList() {
        return new ArrayList<>(
                Arrays.asList(new User("Иван", "Иванов"),
                        new User("Пётр", "Петров"),
                        new User("Михаил", "Михаилов"),
                        new User("Кузьма", "Кузьмин"),
                        new User("Кирилл", "Кириллов")
                )
        );
    }
}
