package com.rurik.jsonandroidpractice.dataclass;



public class User {
    private Integer id;
    private String name;
    private String surname;
    private Address address;

    public User(Integer id, String name, String surname, Address address) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", address=" + address +
                '}';
    }
}
