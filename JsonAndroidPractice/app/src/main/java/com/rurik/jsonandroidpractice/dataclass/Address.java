package com.rurik.jsonandroidpractice.dataclass;



public class Address {

    private String city;
    private String street;
    private Integer house;

    public Address(String city, String street, Integer house) {
        this.city = city;
        this.street = street;
        this.house = house;
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", house=" + house +
                '}';
    }
}
