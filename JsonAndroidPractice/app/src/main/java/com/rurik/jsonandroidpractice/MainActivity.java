package com.rurik.jsonandroidpractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.rurik.jsonandroidpractice.coreprog.JsonParser;
import com.rurik.jsonandroidpractice.dataclass.User;

import org.json.JSONException;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "dc.MainActivity";

    private static final String JSON =
            "{\n"
                    + "  \"users\": [\n"
                    + "    {\n"
                    + "      \"id\": 1,\n"
                    + "      \"name\": \"Иван\",\n"
                    + "      \"surname\": \"Иванов\",\n"
                    + "      \"address\": {\n"
                    + "        \"city\": \"Москва\",\n"
                    + "        \"street\": \"Бабаевская\",\n"
                    + "        \"house\": 5\n"
                    + "      }\n"
                    + "    },\n"
                    + "    {\n"
                    + "      \"id\": 2,\n"
                    + "      \"name\": \"Пётр\",\n"
                    + "      \"surname\": \"Петров\",\n"
                    + "      \"address\": {\n"
                    + "        \"city\": \"Киев\",\n"
                    + "        \"street\": \"Азовская\",\n"
                    + "        \"house\": 23\n"
                    + "      }\n"
                    + "    },\n"
                    + "    {\n"
                    + "      \"id\": 3,\n"
                    + "      \"name\": \"Максим\",\n"
                    + "      \"surname\": \"Максимов\",\n"
                    + "      \"address\": {\n"
                    + "        \"city\": \"Минск\",\n"
                    + "        \"street\": \"Ленина\",\n"
                    + "        \"house\": 12\n"
                    + "      }\n"
                    + "    }\n"
                    + "  ]\n"
                    + "}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        JsonParser parser = new JsonParser();

        try {
            ArrayList<User> users = parser.getUsers(JSON);
            Log.d(LOG_TAG, users.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
