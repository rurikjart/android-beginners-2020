package com.rurik.jsonandroidpractice.coreprog;

import com.rurik.jsonandroidpractice.dataclass.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonParser {


    public ArrayList<User> getUsers(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray users = jsonObject.getJSONArray("users");
        ArrayList<User> result = new ArrayList<>();

        for (int i = 0; i < users.length(); i++) {
            JSONObject userJson = users.getJSONObject(i);
            String name = userJson.getString("name");
            String surname = userJson.getString("surname");
            int id = userJson.getInt("id");

            JSONObject addressJson = userJson.getJSONObject("address");
            String city = addressJson.getString("city");
            String street = addressJson.getString("street");
            int house = addressJson.getInt("house");

            Address address = new Address(city, street, house);
            User user = new User(id, name, surname, address);
            result.add(user);
        }

        return result;
    }

}
