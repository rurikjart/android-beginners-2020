package com.rurik.weatherloaderhttp23;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.rurik.weatherloaderhttp23.network.HttpClient;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private final String LOG_TAG = "dc.MainActivity";

    private HttpClient httpClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        httpClient = new HttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String weather = httpClient.getWeather("London");
                    Log.d(LOG_TAG, weather);

                } catch (IOException e) {
                   e.printStackTrace();
              }
            }
        }).start();


    }
}