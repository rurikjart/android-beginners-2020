package com.rurik.imagelibspractice;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main3);

        // получаем код цвета из ресурса для дальнейшего исспользования
        int grayColor = getResources().getColor(R.color.gray);

        // на основании кода создаем цветовое представление
        Drawable placeholder = new ColorDrawable(grayColor);

        //выдергиваем для управление ImageView
        ImageView imageView = findViewById(R.id.user_image_view);
        Picasso.get()
                .load("https://drive.google.com/uc?id=1CiA2EacvyytVuLsw59yzsjipaF72u5Is")
                .placeholder(placeholder)
                .into(imageView);
    }

    }



