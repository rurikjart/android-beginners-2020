package com.rurik.activityintro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "messageKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);
        // установили обработчик нажатия на кнопку
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // запускаем DetailsActivity
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                // отправляем сообщение
                intent.putExtra(EXTRA_MESSAGE,"Привет из MainActivity!");
                startActivity(intent);
            }
        });
    }
}
