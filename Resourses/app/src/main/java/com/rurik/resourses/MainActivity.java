package com.rurik.resourses;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //установка текста через джава код
        TextView textView = findViewById(R.id.text);

        String appName = getResources().getString(R.string.app_name);
        textView.setText(appName + " R");

        // програмная установка фона
        int backgroundColor = getResources().getColor(R.color.background_light);
        textView.setBackgroundColor(backgroundColor);

        // програмнаяустановка цвета текста
        int textColor = getResources().getColor(R.color.text_dark);
        textView.setTextColor(textColor);

        // установка картинки
        ImageView imageView = findViewById(R.id.image_view);
        Drawable image = getResources().getDrawable(R.drawable.android_jetpack);
        imageView.setImageDrawable(image);

        // по заданию установка имени и фамилии через джава код по аналогии
       TextView enterNameTextView = findViewById(R.id.enter_name_text_view);
       String enterName = getResources().getString(R.string.enter_name);
       enterNameTextView.setText(enterName);


       TextView enterSurnameTextView = findViewById(R.id.enter_surname_text_view);
       String enterSurname = getResources().getString(R.string.enter_surname);
       enterSurnameTextView.setText(enterSurname);

       // по заданию установка цвета текста по заданию
        int textColorName = getResources().getColor(R.color.black);
        enterNameTextView.setTextColor(textColorName);

        int textColorSurname = getResources().getColor(R.color.gray);
        enterSurnameTextView.setTextColor(textColorSurname);

    }

    public void openSecondActivity(View v){
        Intent intent = new Intent(this, LastActivity.class);
        startActivity(intent);
    }


}
