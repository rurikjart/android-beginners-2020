package com.rurik.viewjavacodepractice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Task1Activity extends AppCompatActivity {

    private EditText counterEditText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task1);

        counterEditText = findViewById(R.id.counter_edit_text);

        Button incrementButton = findViewById(R.id.increment_button);
        incrementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // вытаскиваем содержимое компонента в строковую переменную
                String text = counterEditText.getText().toString();
                //Сконвертировать  строку в число
                int number = Integer.valueOf(text);
                //Арифметическое увеличение
                int newNumber = ++number;
                //сконвертируем обратно в cтроку
                String newString = String.valueOf(newNumber);
                //Возвращаем полученную строку в компонет
                counterEditText.setText(newString);

            }
        });

        Button decrementButton = findViewById(R.id.decrement_button);
        decrementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = counterEditText.getText().toString();
                int number = Integer.valueOf(text);
                int newNumber = --number;

                String newNumberText = String.valueOf(newNumber);
                counterEditText.setText(newNumberText);
            }
        });
    }



    public void onLogButtonClick(View view) {
        Log.d("MainActivity", counterEditText.getText().toString());

    }

    public void onToastButtonClick(View view) {
        Toast.makeText(this, counterEditText.getText().toString(), Toast.LENGTH_SHORT).show();
    }
}
