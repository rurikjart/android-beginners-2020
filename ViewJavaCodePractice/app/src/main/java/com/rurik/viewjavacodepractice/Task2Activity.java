package com.rurik.viewjavacodepractice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Task2Activity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task2);

        final View progressBar = findViewById(R.id.progress_bar);
        final Button showHideButton = findViewById(R.id.show_hide_button);

        showHideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isVisible = progressBar.getVisibility() == View.VISIBLE;
                int newVisibility = isVisible ? View.GONE : View.VISIBLE;
                progressBar.setVisibility(newVisibility);

                int textId = newVisibility == View.VISIBLE ? R.string.hide : R.string.show;
                showHideButton.setText(textId);
            }
        });
    }
}
