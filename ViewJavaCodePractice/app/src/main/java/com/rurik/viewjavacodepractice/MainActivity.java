package com.rurik.viewjavacodepractice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button ButtonTask2 = findViewById(R.id.task2);
        ButtonTask2.setOnClickListener(new View.OnClickListener() {
            @Override
            //второе вью открываем через клик листенер для тренировки
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task2Activity.class);
                startActivity(intent);
            }
        });

    }

    public void openTask1Activity(View v){
        Intent intent = new Intent(this, Task1Activity.class);
        startActivity(intent);
    }
}
