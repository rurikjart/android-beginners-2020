package com.rurik.activityintropractice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        String login = intent.getStringExtra(MainActivity.EXTRA_LOGIN);
        String password = intent.getStringExtra(MainActivity.EXTRA_PASSWORD);

        TextView loginTextView = findViewById(R.id.login_text_view);
        loginTextView.setText(login);
        TextView passTextView = findViewById(R.id.password_text_view);
        passTextView.setText(password);
    }
}
