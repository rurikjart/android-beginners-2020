package com.rurik.recyclerviewpractice;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PhoneAdapter extends RecyclerView.Adapter<PhoneAdapter.PhoneViewHolder>{

    private static final String LOG_TAG = "dc.PhoneAdapter";

    // создаем внутренний класс для хранения и отображения информации из источника данных
    public class PhoneViewHolder extends RecyclerView.ViewHolder{
        private TextView phoneNameTextView;
        private TextView phoneVendorTextView;

        public PhoneViewHolder(@NonNull View itemView) {
            super(itemView);

            phoneNameTextView = itemView.findViewById(R.id.phone_name_text_view);
            phoneVendorTextView = itemView.findViewById(R.id.vendor_name_text_view);

            //обработка клика по элементу списка

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int clickedPosition = getLayoutPosition();

                    Phone phone = phones.get(clickedPosition);

                    // Осталось как-то передать эту информацию в Activity
                    onPhoneClickListener.onPhoneClick(phone);
                }
            });

        }

        // метод для отображения полей для объекта телефон
        public void bind(Phone phone) {
            phoneNameTextView.setText(phone.getPhoneName());
            phoneVendorTextView.setText(phone.getPhoneVendor());
        }
    }

    // создаем локально список для объектов
    private List<Phone> phones;
    // переменная слушателя
    private OnPhoneClickListener onPhoneClickListener;

    // создаем интерфейс определяющий нажатие на элемент списка
    public interface OnPhoneClickListener {
        void onPhoneClick(Phone phone);
    }

    // создаём конструктор Adapter, передавая список телефонов; интерфейс конструктора дополнен слушателем
    //OnPhoneClickListener
    public PhoneAdapter(List<Phone> phones, OnPhoneClickListener onPhoneClickListener) {

        this.phones = phones;
        this.onPhoneClickListener = onPhoneClickListener;

    }

   //далее переопределяем обязательные методы для адаптера


    @NonNull
    @Override
    public PhoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // достаем объект элемента списка из XML макета и создаем на его основании програмный View Объект
        //предварительно определяем текущий контекст
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View phoneItemView = inflater.inflate(R.layout.phone_item_view, parent, false);

        // Создаём объект phoneViewHolder для хранения и использования в объекте адаптере элемента списка
        PhoneViewHolder viewHolder = new PhoneViewHolder(phoneItemView);

         //логируем создание объекта контроля хеш суммы
        Log.d(LOG_TAG, "onCreateViewHolder #" + viewHolder.hashCode());


        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull PhoneViewHolder phoneViewHolder, int position) {

        // Достаём объект phone для текущей позиции
        Phone phone = phones.get(position);

        // Указываем объекту phoneViewHolder, чтобы он отобразил объект с данными Java объект в XML макете
        phoneViewHolder.bind(phone);

        // логируем хеш сумму аобъекта для контроля переиспользования элементов
        Log.d(LOG_TAG, "onBindViewHolder #" + phoneViewHolder.hashCode());

    }

    @Override
    public int getItemCount() {
        // возвращаем количество элементов, чтобы Adapter понимал
        // сколько элементов надо создать
        return phones.size();
    }

    // метод добавления нового элемента списка интерактивно
    public void addItems(List<Phone> newPhones) {
        phones.addAll(newPhones);
        notifyDataSetChanged();
    }
}
