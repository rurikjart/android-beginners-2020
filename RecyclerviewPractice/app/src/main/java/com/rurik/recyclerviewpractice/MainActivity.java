package com.rurik.recyclerviewpractice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // получаем доступ к объекту  RecyclerView в макете activity_main
        RecyclerView phoneRecyclerView = findViewById(R.id.phones_recycler_view);

        // установка объекта LayoutManager - отвечает за форму отображения элементов
        phoneRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //создаем список телефонов
        List<Phone> phones  = createPhoneList();

        //на стороне активити создаем слушатель для определения переданных данных из адаптера
        PhoneAdapter.OnPhoneClickListener listener = new PhoneAdapter.OnPhoneClickListener(){
            @Override
            public void onPhoneClick(Phone phone){
                String fullName = phone.getPhoneName() + " " + phone.getPhoneVendor();
                Toast.makeText(MainActivity.this, fullName, Toast.LENGTH_SHORT).show();
            }
        };


        // создаём Adapter для визуального компонента, передавая список телефонов и листерер слушатель
        final PhoneAdapter adapter = new PhoneAdapter(phones, listener);

        // соединяем RecyclerView и Adapter
            phoneRecyclerView.setAdapter(adapter);


        // выстраивам отображение данных
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        phoneRecyclerView.setLayoutManager(layoutManager);


        // обработака по нажатию кнопки, для добавления нового элемента списка и отрисовки элемента

        Button addItemsButton = findViewById(R.id.add_items_button);
        addItemsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Phone>  newPhones = createPhoneList();
                adapter.addItems(newPhones);
            }
        });


    }



    // Метод для создания списка телефонов

    private List<Phone> createPhoneList() {

        return new ArrayList<>(
                Arrays.asList(new Phone("Iphone XS", "Apple"),
                        new Phone("MiA2", "Xiaomi"),
                        new Phone("Galaxy Note 9", "Samsung"),
                        new Phone("MATE 20 PRO", "Huawei"),
                        new Phone("PIXEL 3", "Google"),
                        new Phone("P20 PRO", "Huawei")
                )
        );

    }
}
