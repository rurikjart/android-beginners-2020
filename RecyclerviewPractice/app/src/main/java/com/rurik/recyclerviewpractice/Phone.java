package com.rurik.recyclerviewpractice;

import java.util.Objects;

public class Phone {
    private String phoneName;
    private String phoneVendor;



    public Phone(String phoneName, String phoneVendor) {
        this.phoneName = phoneName;
        this.phoneVendor = phoneVendor;
    }

    public String getPhoneName() {
        return phoneName;
    }



    public String getPhoneVendor() {
        return phoneVendor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Phone phone = (Phone) o;

        if(!phoneName.equals(phone.phoneName)) return false;

        return phoneVendor.equals(phone.phoneVendor);

    }

    @Override
    public int hashCode() {

        int result = phoneName.hashCode();

        result = 31 * result + phoneVendor.hashCode();

        return result;

    }
}
