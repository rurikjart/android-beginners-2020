package com.rurik.imagelibs;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class Main4Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        // тянем отображалку
        ImageView imageView = findViewById(R.id.image_view);
        // тянем код цвета который будем использовать
        int color = getResources().getColor(R.color.gray);
        //используем код цвета
        Drawable placeholder = new ColorDrawable(color);



        Picasso.get()
                .load("https://drive.google.com/uc?id=1_i7y7arOdr3El7R7PyH21NVVHLHPRLNC")
                .placeholder(placeholder)
                .into(imageView);

    }
}
