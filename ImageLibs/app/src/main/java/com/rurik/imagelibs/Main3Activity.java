package com.rurik.imagelibs;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        ImageView imageView = findViewById(R.id.image_view);

        Picasso.get()
                .load("https://drive.google.com/uc?id=1_i7y7arOdr3El7R7PyH21NVVHLHPRLNC")
                .placeholder(R.drawable.ic_image_black_24dp)
                .into(imageView);


    }
}
