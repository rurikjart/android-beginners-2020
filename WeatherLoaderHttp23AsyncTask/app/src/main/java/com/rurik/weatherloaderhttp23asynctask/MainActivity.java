package com.rurik.weatherloaderhttp23asynctask;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.rurik.weatherloaderhttp23asynctask.network.HttpClient;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private final String LOG_TAG = "dc.MainActivity";

    private HttpClient httpClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        httpClient = new HttpClient();

        new GetWeatherAsyncTask().execute("London");

    }

    class GetWeatherAsyncTask extends AsyncTask<String, Integer, String> {

        protected void onPreExecute() {
            // выполняется в UI потоке перед тем, как будет выполнен метод doInBackground.
            // Обычно в нём отображается диалог для отображения прогресса
        }

        protected String doInBackground(String... cities) {
            // выполняется в фоновом ( Background ) потоке. В нём необходимо выполнять тяжёлые операции:
            // http запросы, запросы к базе данных, работу с файлами

            try {
                String city = cities[0];
                final String weather = httpClient.getWeather(city);
                return  weather;
            } catch (IOException e) {
                e.printStackTrace();
                return  null;
            }
        }

        protected void onProgressUpdate(Integer... values) {
            // выполняется в UI потоке при явном вызове метода publishProgress().
            // Обычно вызывается во время выполнения doInBackground(), когда вы хотите отобразить,
            // сколько времени или процентов от общего времени осталось ждать до завершения задачи
        }

        protected void onPostExecute(String result) {
            // выполняется в UI потоке после того, как выполнился метод doInBackground().
            // Принимает входной параметр - значение, которое было передано из метода doInBackground()

            if (result != null) {
                //успешный ответ
                Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
            } else {
                // Ошибка
                Toast.makeText(MainActivity.this,"Что-то пошло не так...", Toast.LENGTH_LONG).show();
            }



        }
    }


}
