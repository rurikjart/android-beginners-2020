package com.rurik.activitylifecycle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    private static final String IS_FIRST_TIME_CREATED = "isFirstTimeCreated";

    private boolean isFirstTimeCreated = true;

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        // сохраняем состояние переменной isFirstTimeCreated
        outState.putBoolean(IS_FIRST_TIME_CREATED, isFirstTimeCreated);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Activity создана
        setContentView(R.layout.activity_main);

       if(savedInstanceState != null){
           // восстанавливаем состояние из объекта savedInstanceState
           isFirstTimeCreated = savedInstanceState.getBoolean(IS_FIRST_TIME_CREATED);
       } else {
           // Activity не воссоздаётся - инициализируем значения
       }

        //Введено логирование
        Log.d("devcolibri.MainActivity", "onCreate() isFirstTimeCreated = " + isFirstTimeCreated);
        isFirstTimeCreated = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Activity скоро станет видимой пользователю.

    }


    @Override
    protected void onResume() {
        super.onResume();
        // Activity стала видима пользователю.

    }

    @Override
    protected void onPause() {
        super.onPause();
        // Другая Activity или `View` находится в фокусе
        // (эта Activity приостановлена).
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Activity не видна пользователю (сейчас она остановлена)
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // Activity перезапускается.
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Activity уничтожается
    }
}
