package com.rurik.constraintlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnPE = findViewById(R.id.button_pe);

        btnPE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PEActivity.class);
                startActivity(intent);
            }
        });

        Button btnBIAS = findViewById(R.id.button_bias);

        btnBIAS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task1BiasActivity.class);
                startActivity(intent);
            }
        });

        Button btnBL = findViewById(R.id.button_bl);

        btnBL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task2BaselineActivity.class);
                startActivity(intent);
            }
        });

        Button btnCh = findViewById(R.id.button_ch);

        btnCh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ChainsActivity.class);
                startActivity(intent);
            }
        });

        Button btnBR = findViewById(R.id.button_br);

        btnBR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BarriersActivity.class);
                startActivity(intent);
            }
        });

        Button btnT1ChAct = findViewById(R.id.button_ch_el);

        btnT1ChAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task1ChainsActivity.class);
                startActivity(intent);
            }
        });

        Button btnT2WeightChain = findViewById(R.id.button_ch_waight);

        btnT2WeightChain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task2WeightChainActivity.class);
                startActivity(intent);
            }
        });

        Button btnT3Barrier = findViewById(R.id.button_barrier);

        btnT3Barrier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task3BarrierActivity.class);
                startActivity(intent);
            }
        });

    }






}
