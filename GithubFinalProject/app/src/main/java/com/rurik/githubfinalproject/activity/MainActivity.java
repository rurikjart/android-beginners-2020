package com.rurik.githubfinalproject.activity;


import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rurik.githubfinalproject.R;
import com.rurik.githubfinalproject.adapter.RepositoryAdapter;
import com.rurik.githubfinalproject.entity.Repository;
import com.rurik.githubfinalproject.network.HttpClient;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "dc.MainActivity";
    private HttpClient httpClient;
    private RepositoryAdapter repositoryAdapter;
    private EditText queryEditText;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initRecyclerView();

        // дергаем поле ввода для использование его в коде
        queryEditText = findViewById(R.id.query_edit_text);
        // дергаем прогресс бар для програмного управления им из кода
        progressBar = findViewById(R.id.progress_bar);

        httpClient = new HttpClient();

      //  new GetRepositoriesAsyncTask().execute("android");

    }

    private void initRecyclerView() {
        // связываем для использования компонент
        // получаем доступ к объекту  RecyclerView в макете activity_main
        RecyclerView recyclerView = findViewById(R.id.repository_recycler_view);

        // установка объекта LayoutManager - отвечает за форму отображения элементов
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // установка слушателя с обработчиком обработчика на стороне мейн активити и реализация интерфейса
        RepositoryAdapter.OnRepoClickListener listener = new RepositoryAdapter.OnRepoClickListener() {
            @Override
            public void onRepoClick(Repository repository) {
                Intent intent = new Intent(MainActivity.this, RepoDetailsActivity.class);
                intent.putExtra(RepoDetailsActivity.EXTRA_REPO_NAME, repository.getName());
                String userLogin = repository.getOwner().getLogin();
                intent.putExtra(RepoDetailsActivity.EXTRA_USER_LOGIN, userLogin);
                startActivity(intent);
            }
        };


        repositoryAdapter = new RepositoryAdapter(listener);
        // соединяем RecyclerView и Adapter
        recyclerView.setAdapter(repositoryAdapter);
    }


    public void searchRepositories(View v) {
        String query = queryEditText.getText().toString();
        if(query.isEmpty()) {
            Toast.makeText(this, R.string.empty_text, Toast.LENGTH_SHORT).show();
        } else {
            repositoryAdapter.clearItems();
            new GetRepositoriesAsyncTask().execute(query);
        }
    }

    private class GetRepositoriesAsyncTask extends AsyncTask<String, Void, ArrayList<Repository>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Repository> doInBackground(String... queries) {
           try {

               return httpClient.getRepositories(queries[0]);

           } catch (IOException | JSONException e) {

               e.printStackTrace();
               return  null;

           }
        }

        @Override
        protected void onPostExecute(ArrayList<Repository> result_queries) {

            progressBar.setVisibility(View.GONE);

            if (result_queries != null) {
                //Log.d(LOG_TAG, result_queries.toString());

                repositoryAdapter.addItems(result_queries);
            } else {
                Toast.makeText(MainActivity.this, R.string.error_msg, Toast.LENGTH_SHORT).show();
            }
        }


    }
}
