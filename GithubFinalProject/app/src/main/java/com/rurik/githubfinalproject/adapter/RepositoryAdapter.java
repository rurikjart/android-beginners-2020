package com.rurik.githubfinalproject.adapter;



import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rurik.githubfinalproject.R;
import com.rurik.githubfinalproject.entity.Repository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder> {
            private List<Repository> repositories = new ArrayList<>();
            private OnRepoClickListener onRepoClickListener;


    //  создаем конструктор
    public RepositoryAdapter(OnRepoClickListener onRepoClickListener) {
        this.onRepoClickListener = onRepoClickListener;
    }

    // описание интерфейса
    public interface OnRepoClickListener {
        void onRepoClick(Repository repository);
    }

    @NonNull
    @Override
    public RepositoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // достаем объект элемента списка из XML макета и создаем на его основании програмный View Объект
        //предварительно определяем текущий контекст
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.repository_item_view, parent, false);

        // Создаём объект repositoryViewHolder для хранения и использования в объекте адаптере элемента списка
        return new RepositoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RepositoryViewHolder repositoryViewHolder, int position) {
        // Достаём объект phone для текущей позиции
        Repository repository = repositories.get(position);
        // Указываем объекту repositoryViewHolder, чтобы он отобразил объект с данными Java объект в XML макете
        repositoryViewHolder.bind(repository);
    }

    @Override
    public int getItemCount() {
        // возвращаем количество элементов, чтобы Adapter понимал
        // сколько элементов надо создать
        return repositories.size();
    }

    // метод добавления нового элемента списка интерактивно
    public void addItems(List<Repository> items) {
        repositories.addAll(items);
        notifyDataSetChanged();
    }

    // отчистка списка репозиториев в main activity
    public void clearItems() {
        repositories.clear();
        notifyDataSetChanged();
    }

    // внутренний класс
    class RepositoryViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView descriptionTextView;
        private ImageView ownerImageView;

        public RepositoryViewHolder(@NonNull View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.name_text_view);
            descriptionTextView = itemView.findViewById(R.id.description_text_view);
            ownerImageView = itemView.findViewById(R.id.owner_image_view);

            // так же в конструкторе опрелеляем нажатие по элементу списка
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Repository repository = repositories.get(getLayoutPosition());
                    onRepoClickListener.onRepoClick(repository);
                }
            });

        }

        void bind(Repository repository) {
            nameTextView.setText(repository.getName());
            descriptionTextView.setText(repository.getDescription());

            String avatarUrl = repository.getOwner().getAvatarUrl();
            Picasso.get().load(avatarUrl).placeholder(R.drawable.ic_person_black_24dp).into(ownerImageView);
        }

    }

}
