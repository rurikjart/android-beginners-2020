package com.rurik.jsonandroid.dataclass;

public class Weather {
    private int id;
    private String description;
    private String icon;

    public Weather(int id, String description, String icon) {
        this.id = id;
        this.description = description;
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "Weather{" + "id=" + id + ", description='" + description + '\'' + ", icon='" + icon + '\'' + '}';
    }
}
