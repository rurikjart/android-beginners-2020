package com.rurik.jsonandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

   public void openJSONObjectActivity(View v) {
       Intent intent = new Intent(this, JsonObjectActivity.class);
       startActivity(intent);

   }


   public void openJSONArrayActivity(View v) {
       Intent intent = new Intent(this, JsonArrayActivity.class);
       startActivity(intent);
   }

}
