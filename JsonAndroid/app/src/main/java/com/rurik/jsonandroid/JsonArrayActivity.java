package com.rurik.jsonandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.rurik.jsonandroid.dataclass.*;

public class JsonArrayActivity extends AppCompatActivity {

    private static final String LOG_TAG = "dc.JsonArrayActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_array);

        String jsonString = "{\n"
                + "  \"weather\": [\n"
                + "    {\n"
                + "      \"id\": 500,\n"
                + "      \"main\": \"Rain\",\n"
                + "      \"description\": \"light rain\",\n"
                + "      \"icon\": \"10d\"\n"
                + "    },\n"
                + "\n"
                + "    {\n"
                + "      \"id\": 501,\n"
                + "      \"main\": \"Smoke\",\n"
                + "      \"description\": \"smoke\",\n"
                + "      \"icon\": \"50n\"\n"
                + "    }\n"
                + "  ]\n"
                + "}";

        try {
            //работа с вложенным массивом
            JSONObject jsonObject = new JSONObject(jsonString);

            JSONArray weatherArray = jsonObject.getJSONArray("weather");
            ArrayList<Weather> result = new ArrayList<>();

            for (int i = 0; i < weatherArray.length(); i++) {
                JSONObject object = weatherArray.getJSONObject(i);

                // достаём все поля из Json
                int id = object.getInt("id");
                String description = object.getString("description");
                String icon = object.getString("icon");

                // создаём объект Weather
                Weather weather = new Weather(id, description, icon);
                // добавляем его в ArrayList
                result.add(weather);
            }
            // выводим результат в лог
            Log.d(LOG_TAG, result.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
