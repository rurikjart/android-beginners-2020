package com.rurik.jsonandroid.dataclass;

public class City {

    private int id;
    private String name;
    private int cod;

    private Coordinates coordinates;

    public City(int id, String name, int cod, Coordinates coordinates) {
        this.id = id;
        this.name = name;
        this.cod = cod;
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "City{" + "id=" + id + ", name='" + name + '\'' + ", cod=" + cod + ", coordinates=" + coordinates + '}';
    }

}
