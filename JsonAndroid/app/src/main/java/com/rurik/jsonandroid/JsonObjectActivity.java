package com.rurik.jsonandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.rurik.jsonandroid.dataclass.*;


import org.json.JSONException;
import org.json.JSONObject;

public class JsonObjectActivity extends AppCompatActivity {

    private static final String LOG_TAG = "dc.JsonObjectActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_object);

        String jsonString = "{\n"
                + "  \"id\": 2643743,\n"
                + "  \"name\": \"London\",\n"
                + "  \"cod\": 200,\n"
                + "\n"
                + "  \"coord\": {\n"
                + "    \"lon\": -0.13,\n"
                + "    \"lat\": 51.51\n"
                + "  }\n"
                + "}";


            try {

                //парсинг JSON
                JSONObject jsonObject = new JSONObject(jsonString);

                String name = jsonObject.getString("name");
                int id = jsonObject.getInt("id");
                int cod = jsonObject.getInt("cod");

                // работа с вложенными объектами на уровень ниже
                JSONObject cord = jsonObject.getJSONObject("coord");
                int lon = cord.getInt("lon");
                int lat = cord.getInt("lat");




                Log.d(LOG_TAG, "id=" + id + ", name=" + name + ", cod=" + cod + ", lon =" + lon + ", lat=" + lat);

                //Использование Data Class для хранения информации на время выполнения программы
                Coordinates coordinates = new Coordinates(lon, lat);
                City city = new City(id, name, cod, coordinates);

                Log.d(LOG_TAG, city.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }




    }
}
