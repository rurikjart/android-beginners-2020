package com.rurik.jsonandroid.dataclass;

public class Coordinates {
    private double lon;
    private double lat;

    public Coordinates(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    @Override
    public String toString() {
        return "Coordinates{" + "lon=" + lon + ", lat=" + lat + '}';
    }
}
